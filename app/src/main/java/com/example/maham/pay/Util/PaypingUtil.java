package com.example.maham.pay.Util;

import android.app.Activity;
import android.content.Intent;
import android.net.http.SslError;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

//import wiadevelopers.com.payping.Constant;


public class PaypingUtil
{
    Activity activity;
    Class reDirectedActivity;

    public void play(final WebView w, final String url, final Activity activity, final Class reDirectedActivity)
    {

        this.activity = activity;
        this.reDirectedActivity = reDirectedActivity;
        w.getSettings().setJavaScriptEnabled(true);


        w.addJavascriptInterface(new javaScript(), "cc");

        w.getSettings().setDomStorageEnabled(true);


        w.setWebViewClient(new WebViewClient()
        {


            @Override
            public void onPageFinished(WebView view, String url)
            {

                String title = "پرداخت موفق - پی پینگ";
                if (view.getTitle().equals(title))
                {
                    String send = "javascript:window.cc.show(document.getElementsByTagName('td')[0].innerHTML + \"#\" + document.getElementsByTagName('td')[1].innerHTML + \"#\" + document.getElementsByTagName('td')[2].innerHTML);";
                    w.loadUrl(send);
                }
            }


            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error)
            {
                handler.proceed();

            }


        });

        w.loadUrl(url);
    }

    public static String generateLink(String userName, int price, String name, String description)
    {
        String url = ("https://www.payping.ir/" + userName + "/" + price + "?name=" + name + "&des=" + description);
        return url;
    }

    class javaScript
    {

        @JavascriptInterface
        public void show(String htmlContent)
        {
            htmlContent = htmlContent.replace("\n", "");
            String[] res = htmlContent.split("#");
            for (int i = 0; i < res.length; i++)
                res[i] = res[i].trim();


            JSONObject obj = new JSONObject();
            try
            {
                obj.put("tprice", res[0]);
                obj.put("tnum", res[1]);
                obj.put("texplain", res[2]);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            Intent intent = new Intent(activity, reDirectedActivity);
            intent.putExtra("tdata", obj.toString());
            activity.startActivity(intent);
            activity.finish();
        }
    }
}