package com.example.maham.pay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;

import com.example.maham.pay.Util.PaypingUtil;

public class Activity_Pay extends AppCompatActivity {


    WebView pay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__pay);

        pay = findViewById(R.id.web_view);
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
    //    Toast.makeText(getApplicationContext(),url,Toast.LENGTH_LONG).show();

      // pay.loadUrl(url);   //<- If you use this line, your payment will be made to the browser

        PaypingUtil paypingUtil = new PaypingUtil();
        paypingUtil.play(pay,url,Activity_Pay.this,Final.class);

    }
}
